# Nom du projet
Rétro-engeneering d'un boîtier connecté

## Objectif
Les objectifs à atteindre durant le TP sont : 
-	Comprendre comment fonctionne le module bleak de python 
-	Obtenir et comprendre les différents services et caractéristiques du boîtier
-	Obtenir les fichiers contenus dans le boîtier 
-	Récupérer un trajet GPS dans le boîtier


# Comment démarrer le projet ? 
Pour démarrer le projet, il faut : 
1. Disposer d'un boîtier réaliser par l'entreprise Fluidy ayant pour code 5376
2. Installer le module bleak (version 0.14.0) sur python
3. Exécuter le code contenu dans le fichier code.py

Pour disposer de toutes les fonctionnalités du projet, entre autre les différents services, vous devez également avoir l'application nRF Connect sur votre téléphone.

# Le résultat
Vous devriez obtenir 4 fichiers qui correspondent aux fichiers présents dans le boîtier

# "Logigramme" du code 

1. main()           -> Découverte des appareils disponibles

2. liste(adress)    -> Liste des services et caractéristiques du boîtier

3. main(adress)     -> Lecture des caractéristiques
                    -> Récupération des fichiers du boîtier grâce aux callbacks
 
J'ai dû déclarer la fonction start_loop(loop) sinon ça ne marchait pas (ne pas en prendre compte)


