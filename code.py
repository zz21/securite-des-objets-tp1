import asyncio
import threading
from bleak import BleakScanner
from bleak import BleakClient
from binascii import crc32

#recherche des appareils disponibles

async def main():
    devices = await BleakScanner.discover()
    for d in devices:
        print(d)
        
def start_loop(loop):
    asyncio.set_event_loop(loop)
    loop.run_until_complete(main())

loop = asyncio.new_event_loop()
thread = threading.Thread(target=start_loop, args=(loop,))
thread.start()
thread.join()

#Connexion à un appareil bluetooth
address = "EA:E4:13:42:C7:B7" #Trouvée grâce à la fonction precedente

tab_handle = [] #tableau qui contient les handle des caractéristiques

async def liste(address):
    async with BleakClient(address) as client:
        # Obtenir la liste des services
        services = await client.get_services()              #récupération des services
        print("Services disponibles :")
        for service in services:                            #on parcourt les services disponibles
            print(f"- Service {service}")
            characteristics = service.characteristics       #récpération des caractéristiques du service
            print("  Caractéristiques :")           
            for characteristic in characteristics:          #on parcourt toute les caractéritiques du service
                print(f"  - {characteristic}")  
                tab_handle.append(characteristic.handle)    #ajout des handles dans le tableau

def start_loop(loop):
    asyncio.set_event_loop(loop)
    loop.run_until_complete(liste(address))

loop = asyncio.new_event_loop()
thread = threading.Thread(target=start_loop, args=(loop,))
thread.start()
thread.join()

#definition des handles utiles récupérés lors de la phase précédente
handle_LIST = 58
handle_READ = 66
handle_ACK = 100
handle_LOG = 111
handle_NUM = 62

#definition des noms de fichiers récupérés dans la fonction suivante
name_fichier1 = "543130000000000"
name_fichier2 = "543131000000000"
name_fichier3 = "543133000000000"
name_fichier4 = "543533373600000"

#definition de la partie chunk à envoyer avec le nom du fichier 
end_data = "00000000009090909"

i = 0
    
async def main(address):
    async with BleakClient(address) as client:
        for k in range (len(tab_handle)):                           #on parcourt le tableau d'handles
            try:
                handle = tab_handle[k]                              
                model_number = await client.read_gatt_char(handle)  #on lit les valeurs des caractéristiques grâce à l'handle 
                print(handle)                                       #(seulement ceux avec les droits de lecture)
                print(model_number)
            except :
                print("pas la permission de lire")
        await client.start_notify(handle_LIST, callback)            #on s'abonne aux caractéristques LIST, READ, ACK et LOG
        await client.start_notify(handle_READ, callback2)
        await client.start_notify(handle_ACK, callback)
        await client.start_notify(handle_LOG, callback)
        
        nb_fichiers = await client.read_gatt_char(handle_NUM)       #lecture du nombre de fichier dans la caractéristique NUM
        await client.write_gatt_char(handle_LIST,nb_fichiers)       #ecriture de NUM dans LIST
        await asyncio.sleep(5)
        
        await client.write_gatt_char(66,bytearray.fromhex(name_fichier1+end_data))  #ecriture dans READ de la données récupérée pour le fichier 1
        await asyncio.sleep(20)
        global i
        i=i+1
        await client.write_gatt_char(66,bytearray.fromhex(name_fichier2+end_data))  #idem pour le fichier 2
        await asyncio.sleep(20)
        i=i+1
        await client.write_gatt_char(66,bytearray.fromhex(name_fichier3+end_data))  #idem pour le fichier 3
        await asyncio.sleep(20)
        i=i+1
        await client.write_gatt_char(66,bytearray.fromhex(name_fichier4+end_data))  #idem pour le fichier 4
        await asyncio.sleep(20)

def callback(sender, data):
        print(f"{sender}:{data}")
        print(f"en hexa : {sender}:{data.hex()}")
        print(" ")


#Cette fonction permet d'écrire de récupérer les 4 fichiers après écriture dans READ
def callback2(sender,data):
    name = data[0:8].hex()
    print("le nom est ",name)
    if(i==0): 
        fichier = open('fichier1','ab+')
        fichier.write(data)
    elif(i==1):
        fichier = open('fichier2','ab+')
        fichier.write(data)
    elif(i==2):
        fichier = open('fichier3','ab+')
        fichier.write(data)
    elif(i==3):
        fichier = open('fichier4','ab+')
        fichier.write(data)

def start_loop(loop):
    asyncio.set_event_loop(loop)
    loop.run_until_complete(main(address))

loop = asyncio.new_event_loop()
thread = threading.Thread(target=start_loop, args=(loop,))
thread.start()
thread.join()




